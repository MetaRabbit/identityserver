﻿using IdentityServer4.Models;

namespace IdentityServer.UI.Error
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}