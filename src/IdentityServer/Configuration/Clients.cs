﻿using System.Collections.Generic;
using IdentityServer4.Models;

namespace IdentityServer.Configuration
{
    public class Clients
    {
        public static IEnumerable<Client> Get()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Hybrid Client",
                    AllowedGrantTypes = GrantTypes.Hybrid,

                    RedirectUris = new List<string>
                    {
                        "http://localhost:58053/signin-oidc"
                    },

                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost:58053/"
                    },

                    ClientSecrets = new List<Secret>
                    {
                        new Secret("motherfuckermvcclient".Sha256())
                    },

                    AllowedScopes = new List<string>
                    {
                        StandardScopes.OpenId.Name,
                        StandardScopes.Profile.Name,
                        StandardScopes.OfflineAccess.Name,
                        "botagentreadapi",
                        "gameinfoapi"
                    },
                    //8 Hours
                    AccessTokenLifetime = 28800
                },
                new Client
                {
                    ClientId = "BotAgent",
                    ClientName = "BotAgent",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets = new List<Secret>
                    {
                        new Secret("wowoiverycoolbotagentnnn".Sha256())
                    },

                    AllowedScopes = new List<string>
                    {
                        StandardScopes.OpenId.Name,
                        StandardScopes.OfflineAccess.Name,
                        "botagentreadapi",
                        "botagentwriteapi",
                        "gameinfoapi"
                    },
                     //8 Hours
                    AccessTokenLifetime = 28800
                },
                 new Client
                {
                    ClientId = "Updater",
                    ClientName = "Updater",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets = new List<Secret>
                    {
                        new Secret("hgfh4bbds2ruhdg".Sha256())
                    },

                    AllowedScopes = new List<string>
                    {
                        StandardScopes.OfflineAccess.Name,
                        "botagentreadapi",
                        "botagentwriteapi"

                    },
                     //8 Hours
                    AccessTokenLifetime = 28800
                }

            };
        }
    }
}