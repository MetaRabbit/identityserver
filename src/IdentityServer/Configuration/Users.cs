﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Services.InMemory;

namespace IdentityServer.Configuration
{
    static class Users
    {
        public static List<InMemoryUser> Get()
        {
            var users = new List<InMemoryUser>
            {
                new InMemoryUser{Subject = "818727", Username = "Zhenya", Password = "sda323asd12bnjdfsDF", 
                    Claims = new Claim[]
                    {
                        new Claim(JwtClaimTypes.Role, "Admin"),
                        new Claim(JwtClaimTypes.Role, "Developer")
                        
                    }
                },
                new InMemoryUser{Subject = "88421113", Username = "Dima", Password = "fds3Fdfs3mfdfFffds", 
                    Claims = new Claim[]
                    {
                        new Claim(JwtClaimTypes.Role, "Admin"),
					    new Claim(JwtClaimTypes.Role, "Developer"),
                        new Claim(JwtClaimTypes.Role, "Bot Agent Developer")
                    }
                },
                 new InMemoryUser{Subject = "88421228", Username = "botagent88421228", Password = "adad12dasda21dasDS",
                    Claims = new Claim[]
                    {
                      
                    }
                }
            };

            return users;
        }
    }
}