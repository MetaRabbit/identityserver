﻿using System.Collections.Generic;
using IdentityServer4.Models;

namespace IdentityServer.Configuration
{
    public class Scopes
    {
        public static IEnumerable<Scope> Get()
        {
            return new List<Scope>
            {
                StandardScopes.OpenId,
                StandardScopes.Profile,
                StandardScopes.OfflineAccess,

                new Scope
                {
                    Name = "gameinfoapi",
                    DisplayName = "gameinfoapi",
                    Type = ScopeType.Resource
                },
                 new Scope
                {
                    Name = "botagentreadapi",
                    DisplayName = "botagentreadapi",
                    Type = ScopeType.Resource
                },
                  new Scope
                {
                    Name = "botagentwriteapi",
                    DisplayName = "botagentwriteapi",
                    Type = ScopeType.Resource
                }
            };
        }
    }
}